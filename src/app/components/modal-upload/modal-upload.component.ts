import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { SubirArchivoService } from 'src/app/services/service.index';
import { ModalUploadService } from './modal-upload.service';

@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styles: []
})
export class ModalUploadComponent implements OnInit {

  //oculto:string='';
  imagenSubir:File;
  imagenTemp:string | ArrayBuffer ;
  
  constructor(public _subirArchivoService:SubirArchivoService,public _modalUploadService:ModalUploadService) { 
    //console.log("Modal listo");    
  }


  ngOnInit() {
  }


  seleccionImagen(archivo:File){
    if(!archivo){
      this.imagenSubir=null;
      return;
    }

    if(archivo.type.indexOf('image')<0){
      Swal.fire("Sólo Imagenes","El archivo seleccionado no es una imagén","info");
      this.imagenSubir=null;
      return;
    }

    this.imagenSubir=archivo;
    let reader=new FileReader();
    let urlImagenTemp=reader.readAsDataURL(archivo);
    reader.onloadend=()=>this.imagenTemp=reader.result;
    
    

    this.imagenSubir=archivo;    
    //console.log(event);    
  }

  cerrarModal(){
    this.imagenTemp=null;
    this.imagenSubir=null;
    this._modalUploadService.ocultarModal();
  }

  subirImagen(){
    this._subirArchivoService.subirArchivo(this.imagenSubir,this._modalUploadService.tipo,this._modalUploadService.id)
      .then(resp=>{
          console.log(resp);          
          this._modalUploadService.notificacion.emit(resp);
          //this._modalUploadService.ocultarModal();
          this.cerrarModal();
      }).catch(err=>{
          console.log("Error en la carga");          
      });
  }

}
