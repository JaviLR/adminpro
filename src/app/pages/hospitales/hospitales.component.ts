import { Component, OnInit } from '@angular/core';
import { HospitalService } from 'src/app/services/service.index';
import Swal from 'sweetalert2';
import { Hospital } from 'src/app/models/hospital.model';
import { ModalUploadService } from 'src/app/components/modal-upload/modal-upload.service';

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styles: []
})
export class HospitalesComponent implements OnInit {
  
  hospitales:Hospital[]=[];
  hospital:Hospital;
  desde:number=0;
  totalRegistros:number=0;
  cargando:boolean=true;
  //@ViewChild('txtNombreHospital', {static: true}) txtProgress:ElementRef;

  constructor
  (
    public _hospitalService:HospitalService,
    public _modalUploadService:ModalUploadService
  ) {}

  ngOnInit() {
    this.cargarHospitales();
    this._modalUploadService.notificacion.subscribe(resp=>{
      this.cargarHospitales();
    });
  }

  cargarHospitales(){
    this.cargando=true;
    this._hospitalService.cargarHospitales()
    .subscribe((hospitales:any)=>{
        console.log(hospitales);               
        this.hospitales=hospitales;        
        this.cargando=false;      
    });
  }

  cargarHospital(id:string){
    this._hospitalService.obtenerHospital(id)
    .subscribe((resp:any)=>{
        console.log(resp);              
    });
  }

  borrarHospital(id:string){    
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this hospital!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {          
          this._hospitalService.borrarHospital(id)
          .subscribe(resp=>{
              console.log(resp);
              this.cargarHospitales();              
          });                  
        }
      })    
  }

  async agregarHospital(){

  
  const { value: ipAddress } = await Swal.fire({
    title: 'Enter your IP address',
    input: 'text',    
    showCancelButton: true,
    inputValidator: (value) => {
      if (!value) {
        return 'You need to write something!'
      }
    }
  })

  if (ipAddress) {
    Swal.fire("Hospital creado",ipAddress,"success");
    this.crearHospital(ipAddress);
    this.cargarHospitales();
  }
  }

  crearHospital(nombre:string){
    this._hospitalService.crearHospital(nombre).subscribe(resp=>{
      console.log(resp);        
    });
  }

  buscarHospital(termino:string){
    console.log(termino);   
    if(termino.length<=0){
      this.cargarHospitales();
      return;
    }

    this.cargando=true;

    this._hospitalService.buscarHospital(termino)
      .subscribe((hospitales:Hospital[])=>{
        console.log(hospitales);  
        this.hospitales=hospitales;      
        this.cargando=false;
      });
  }

  actualizarHospital(hospital:Hospital){    
    //console.log(hospital);    
    //this.hospital.nombre=hospital.nombre;    
    this._hospitalService.actualizarHospital(hospital).subscribe();
  }


  mostrarModal(id:string){
    this._modalUploadService.mostrarModal("hospitales",id);
  }





}
