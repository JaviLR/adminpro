import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
import { UsuarioService } from 'src/app/services/service.index';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: []
})
export class ProfileComponent implements OnInit {

  usuario:Usuario;
  imagenSubir:File;
  imagenTemp:string | ArrayBuffer ;

  constructor(public _usuarioService:UsuarioService) { 
    this.usuario=this._usuarioService.usuario;
  }

  ngOnInit() {
  }

  guardar(usuario:Usuario){
    //console.log(usuario); 
    if(!this.usuario.google){
      this.usuario.email=usuario.email;
    }
    this.usuario.nombre=usuario.nombre;    
    this._usuarioService.actualizarUsuario(this.usuario).subscribe();
  }

  seleccionImagen(archivo:File){
    if(!archivo){
      this.imagenSubir=null;
      return;
    }

    if(archivo.type.indexOf('image')<0){
      Swal.fire("Sólo Imagenes","El archivo seleccionado no es una imagén","info");
      this.imagenSubir=null;
      return;
    }

    this.imagenSubir=archivo;
    let reader=new FileReader();
    let urlImagenTemp=reader.readAsDataURL(archivo);
    reader.onloadend=()=>this.imagenTemp=reader.result;
    
    

    this.imagenSubir=archivo;    
    //console.log(event);    
  }

  cambiarImagen(){
    this._usuarioService.cambiarImagen(this.imagenSubir,this.usuario._id);
  }

}

