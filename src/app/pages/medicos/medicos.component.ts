import { Component, OnInit } from '@angular/core';
import { Medico } from 'src/app/models/medico.model';
import { MedicoService } from 'src/app/services/service.index';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-medicos',
  templateUrl: './medicos.component.html',
  styles: []
})
export class MedicosComponent implements OnInit {

  medicos:Medico[]=[];
  constructor(public _medicosService:MedicoService) { }

  ngOnInit() {
    this.cargarMedicos();
  }

  cargarMedicos(){
    this._medicosService.cargarMedicos()
      .subscribe(medicos=>this.medicos=medicos)
    ;
  }

  buscarMedico(termino:string){

    if(termino.length<=0){
        this.cargarMedicos();
        return;
    }

      this._medicosService.buscarMedicos(termino).subscribe(medicos=>this.medicos=medicos);
  }

  borrarMedico(medico:Medico){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this hospital!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {          
        this._medicosService.borrarMedico(medico._id)
        .subscribe(resp=>{
            console.log(resp);
            this.cargarMedicos();              
        });                  
      }
    })
  }

}
