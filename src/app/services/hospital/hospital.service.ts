import { Injectable } from '@angular/core';
import { Hospital } from 'src/app/models/hospital.model';
import {HttpClient} from '@angular/common/http';
import { URL_SERVICIOS } from 'src/app/config/config';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { SubirArchivoService } from '../archivos/subir-archivo.service';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class HospitalService {

  hospital:Hospital;
  token:string;
  totalHospitales:number;

  constructor(
    public http:HttpClient,
    public router:Router,
    public _subirArchivoService:SubirArchivoService,
    public _usuarioService:UsuarioService
    
    ) { 
    //this.cargarStorage();    
  }

  /*cargarStorage(){
    if(localStorage.getItem('token')){
      this.token=localStorage.getItem('token'); 
    }else{
      this.token=""; 
    }
  }*/

  cargarHospitales(){
    let url=URL_SERVICIOS+"/hospital";
    return this.http.get(url).pipe(
      map((resp:any)=>{
          this.totalHospitales=resp.total;
          return resp.hospitales;
      })
    );
  }

  
  obtenerHospital(id:string){  
    let url=URL_SERVICIOS+"/hospital/"+id;
    return this.http.get(url).pipe(map((resp:any)=>resp.hospital));
  }

  borrarHospital(id:string){
    let url=URL_SERVICIOS+"/hospital/"+id;    
    url+='?token='+this._usuarioService.token;
    return this.http.delete(url)
      .pipe(map(resp=>{
        Swal.fire(
          'Deleted!',
          'Hospital eliminado.',
          'success'
        )
        return resp;
      }))
    ;
  }

  crearHospital(nombre:string){
    /*let hospital=new Hospital(
      nombre      
    );*/

    let url=URL_SERVICIOS+"/hospital";    
    url+='?token='+this._usuarioService.token;
    return this.http.post(url,{nombre}).pipe(
      map( (resp: any) => {
        Swal.fire({
          title: 'Bien!',
          text: 'Hospital Creado',
          icon: 'success',
          confirmButtonText: 'Aceptar'
        })
        return resp.hospital;
      })
    );
  }

  buscarHospital(termino:string){
    let url=URL_SERVICIOS+"/busqueda/coleccion/hospitales/"+termino;
    return this.http.get(url).pipe(
      map((resp:any)=>resp.hospitales)
    );
  }

  actualizarHospital(hospital:Hospital){
    let url=URL_SERVICIOS+"/hospital/"+hospital._id;
    url+="?token="+this._usuarioService.token;    
    return this.http.put(url,hospital).pipe(
      map((resp:any)=>{                  
          //let usuarioDB:Hospital=resp.hospital;          
          Swal.fire("Hospital Actualizado",hospital.nombre,"success");
          //return true;
          return resp.hospital;
      })
    );    
  }
  
}
