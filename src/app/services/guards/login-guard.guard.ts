import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
//import { UsuarioService } from '../service.index';


@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {

  constructor(public _usuarioservice:UsuarioService,public router:Router){
  }

  canActivate(){
    if(this._usuarioservice.estaLogueado()){
      //console.log("Paso el guard");      
      return true;
    }else{
      console.log("Bloqueado por el guard");   
      this.router.navigate(['/login']);   
      return false;
    }        
  }
  


}

