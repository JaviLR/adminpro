import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//RUTAS
import { AppRoutingModule } from './app-routing.module';
//import { APP_ROUTES } from './app-routing.module';

//MÓDULOS
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { PagesModule } from './pages/pages.module';

//TEMPORAL
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//SERVICIOS
import { ServiceModule } from './services/service.module';
import { PagesComponent } from './pages/pages.component';
import { SharedModule } from './shared/shared.module';

//import { ImagenPipe } from './pipes/imagen.pipe';

//import { GraficoDonaComponent } from './components/grafico-dona/grafico-dona.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent, 
    RegisterComponent,
    PagesComponent
  ],
  imports: [
    BrowserModule,
    //PagesModule,    
    //APP_ROUTES
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ServiceModule,
    SharedModule
  ],
  providers: [    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



